package com.gameonanil.api_integration.ui.homefragment.tabs

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.gameonanil.api_integration.adapter.FoodRecyclerAdapter
import com.gameonanil.api_integration.data.foodmodel.FoodsItem
import com.gameonanil.api_integration.databinding.FragmentBusinessBinding
import com.gameonanil.api_integration.ui.homefragment.HomeFragmentDirections
import com.gameonanil.api_integration.ui.main.FoodViewModel
import com.gameonanil.api_integration.utils.Resource

class FoodFragment : Fragment(), FoodRecyclerAdapter.MainRecyclerListener {
    private var _binding: FragmentBusinessBinding? = null
    private val binding: FragmentBusinessBinding get() = _binding!!
    private lateinit var mViewModel: FoodViewModel
    private lateinit var mAdapter: FoodRecyclerAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentBusinessBinding.inflate(inflater, container, false)


        mAdapter = FoodRecyclerAdapter(requireActivity(), this)
        mViewModel = ViewModelProvider(requireActivity())[FoodViewModel::class.java]
        binding.mainRecyclerView.adapter = mAdapter


        binding.swipeRefreshLayout.setOnRefreshListener {
            setUpLiveData()
        }


        return binding.root
    }

    override fun onResume() {
        super.onResume()
        setUpLiveData()
    }

    override fun onItemClick(food: FoodsItem, imageView: ImageView) {
        super.onItemClick(food, imageView = imageView)

        findNavController().navigate(
            HomeFragmentDirections.actionHomeFragmentToFoodDetailsFragment(
                id = food.id,
                name = food.name,
                available = food.available,
                address = food.address,
                image = food.image,
                price = food.price,
                rating = food.rating
                ),
        )

    }

    private fun setUpLiveData() {
        mViewModel.fetchFoods().observe(requireActivity()) { response ->
            when (response.status) {
                Resource.Status.SUCCESS -> {
                    Log.d("TAG", "setUpLiveData: ${response.data}")

                    val foodlist = response.data
                    showData(foodlist)
                }
                Resource.Status.ERROR -> {
                    showError(response.exception!!.message.toString())
                }

                Resource.Status.LOADING -> {
                    showLoadingProgress(true)
                }
            }
        }

        mViewModel.fetchRestaurant().observe(requireActivity()) { response ->
            when (response.status) {
                Resource.Status.SUCCESS -> {
                    Log.d("TAG", "setUpLiveData:RES ${response.data}")

                }
                Resource.Status.ERROR -> {

                }

                Resource.Status.LOADING -> {
                    showLoadingProgress(true)
                }
            }
        }
    }

    private fun showData(response: List<FoodsItem>?) {
        binding.swipeRefreshLayout.isRefreshing = false
        binding.errorLayout.visibility = GONE
        binding.mainRecyclerView.visibility = VISIBLE
        showLoadingProgress(false)
        if (response != null) {
            mAdapter.setFoods(response)
        }
    }

    private fun showError(error: String) {
        binding.swipeRefreshLayout.isRefreshing = false
        showLoadingProgress(false)
        binding.mainRecyclerView.visibility = GONE
        binding.errorLayout.visibility = VISIBLE
        binding.errorFragment.tvError.text = error
    }


    private fun showLoadingProgress(isShowing: Boolean) {
        binding.apply {
            if (isShowing) {
                progressCircular.visibility = View.VISIBLE
            } else {
                progressCircular.visibility = View.GONE
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}