package com.gameonanil.api_integration.ui.errorFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.gameonanil.api_integration.databinding.FragmentErrorBinding
import com.gameonanil.api_integration.ui.main.MainViewModel
import com.gameonanil.api_integration.utils.Resource


class ErrorFragment : Fragment() {

    private var _binding: FragmentErrorBinding? = null
    private val binding: FragmentErrorBinding get() = _binding!!
    private lateinit var mViewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentErrorBinding.inflate(inflater,container,false)
        mViewModel = ViewModelProvider(requireActivity())[MainViewModel::class.java]
        binding.swipeRefreshLayout.setOnRefreshListener {
            checkData()
            binding.swipeRefreshLayout.isRefreshing =false
        }

        return binding.root
    }


    private fun showLoadingProgress(isShowing: Boolean) {
        binding.apply {
            if (isShowing) {
                progressCircular.visibility = View.VISIBLE
            } else {
                progressCircular.visibility = View.GONE
            }
        }
    }

    private fun checkData() {
        mViewModel.fetchBusinessList().observe(requireActivity()) { response ->

            when (response.status) {
                Resource.Status.SUCCESS -> {
                    showLoadingProgress(false)
                    findNavController().popBackStack()
                }
                Resource.Status.ERROR  -> {
                    showLoadingProgress(false)
                }

                Resource.Status.LOADING  -> {
                    showLoadingProgress(true)
                }
            }
        }
    }


}