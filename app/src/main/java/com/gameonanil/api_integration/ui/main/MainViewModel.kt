package com.gameonanil.api_integration.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.gameonanil.api_integration.data.network.ArticleApi
import com.gameonanil.api_integration.utils.AppException
import com.gameonanil.api_integration.utils.Constants
import com.gameonanil.api_integration.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

@HiltViewModel
class MainViewModel
@Inject constructor(private val api: ArticleApi) : ViewModel() {
    companion object {
        private const val TAG = "MainViewModel"
    }

    fun fetchBusinessList() = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            val data = api.getArticles(Constants.COUNTRY, Constants.API_KEY,Constants.CATEGORY_BUSINESS)
            emit(Resource.success(data))
        } catch (e: Exception) {
            emit(Resource.error(null, AppException(e)))
        }
    }

    fun fetchSports() = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            val data = api.getArticles(Constants.COUNTRY, Constants.API_KEY,Constants.CATEGORY_SPORTS)
            emit(Resource.success(data))
        } catch (e: Exception) {
            emit(Resource.error(null, AppException(e)))
        }
    }
    fun fetchHealth() = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            val data = api.getArticles(Constants.COUNTRY, Constants.API_KEY,Constants.CATEGORY_HEALTH)
            emit(Resource.success(data))
        } catch (e: Exception) {
            emit(Resource.error(null, AppException(e)))
        }
    }
    fun fetchEntertainment() = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            val data = api.getArticles(Constants.COUNTRY, Constants.API_KEY,Constants.CATEGORY_ENTERTAINMENT)
            emit(Resource.success(data))
        } catch (e: Exception) {
            emit(Resource.error(null, AppException(e)))
        }
    }


}