package com.gameonanil.api_integration.ui.homefragment.tabs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.gameonanil.api_integration.adapter.MainRecyclerAdapter
import com.gameonanil.api_integration.data.model.Article
import com.gameonanil.api_integration.data.model.ArticleResponse
import com.gameonanil.api_integration.databinding.FragmentSportsBinding
import com.gameonanil.api_integration.ui.homefragment.HomeFragmentDirections
import com.gameonanil.api_integration.ui.main.MainViewModel
import com.gameonanil.api_integration.utils.Resource

class SportsFragment : Fragment(), MainRecyclerAdapter.MainRecyclerListener {
    private var _binding: FragmentSportsBinding? = null
    private val binding: FragmentSportsBinding get() = _binding!!
    private lateinit var mViewModel: MainViewModel
    private lateinit var mAdapter: MainRecyclerAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSportsBinding.inflate(inflater, container, false)


        mAdapter = MainRecyclerAdapter(this)
        mViewModel = ViewModelProvider(requireActivity())[MainViewModel::class.java]
        binding.mainRecyclerView.adapter = mAdapter

        binding.swipeRefreshLayout.setOnRefreshListener {
            setUpLiveData()
        }

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        setUpLiveData()
    }

    override fun onItemClick(article: Article, imageView: ImageView) {
        super.onItemClick(article, imageView = imageView)


        findNavController().navigate(
            HomeFragmentDirections.actionHomeFragmentToArticleDetailsFragment(
                author = article.author!!,
                description = article.description!!,
                content = article.content ?: "",
                title = article.title!!,
                url = article.urlToImage,
            ),
        )

    }

    private fun setUpLiveData() {
        mViewModel.fetchSports().observe(requireActivity()) { response ->
            when (response.status) {
                Resource.Status.SUCCESS -> {
                    showData(response)
                }
                Resource.Status.ERROR -> {
//                    val action =
//                        HomeFragmentDirections.actionHomeFragmentToErrorFragment(response.exception!!.message.toString())
//                    findNavController().navigate(action)
                    showError(response.exception!!.message.toString())
                }

                Resource.Status.LOADING -> {
                    showLoadingProgress(true)
                }
            }
        }
    }

    private fun showData(response: Resource<ArticleResponse>) {
        binding.swipeRefreshLayout.isRefreshing = false
        binding.errorLayout.visibility = View.GONE
        binding.mainRecyclerView.visibility = View.VISIBLE
        showLoadingProgress(false)
        mAdapter.setArticle(response.data!!.articles)
    }

    private fun showError(error: String) {
        binding.swipeRefreshLayout.isRefreshing = false
        showLoadingProgress(false)
        binding.mainRecyclerView.visibility = View.GONE
        binding.errorLayout.visibility = View.VISIBLE
        binding.errorFragment.tvError.text = error
    }


    private fun showLoadingProgress(isShowing: Boolean) {
        binding.apply {
            if (isShowing) {
                progressCircular.visibility = View.VISIBLE
            } else {
                progressCircular.visibility = View.GONE
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}