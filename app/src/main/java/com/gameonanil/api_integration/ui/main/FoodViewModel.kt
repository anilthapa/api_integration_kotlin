package com.gameonanil.api_integration.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.gameonanil.api_integration.data.network.FoodApi
import com.gameonanil.api_integration.utils.AppException
import com.gameonanil.api_integration.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

@HiltViewModel
class FoodViewModel
@Inject constructor(private val api: FoodApi) : ViewModel() {
    companion object {
        private const val TAG = "MainViewModel"
    }

    fun fetchFoods() = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            val data = api.getFood()
            emit(Resource.success(data))
        } catch (e: Exception) {
            emit(Resource.error(null, AppException(e)))
        }
    }

    fun fetchRestaurant() = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            val data = api.getRestaurant()
            emit(Resource.success(data))
        } catch (e: Exception) {
            emit(Resource.error(null, AppException(e)))
        }
    }


}