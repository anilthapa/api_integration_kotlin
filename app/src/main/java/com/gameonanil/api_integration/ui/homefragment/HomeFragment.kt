package com.gameonanil.api_integration.ui.homefragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.gameonanil.api_integration.adapter.PagerAdapter
import com.gameonanil.api_integration.databinding.FragmentHomeBinding
import com.google.android.material.tabs.TabLayoutMediator


class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding: FragmentHomeBinding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
       _binding = FragmentHomeBinding.inflate(inflater,container,false)

        binding.apply {
            val adapter = PagerAdapter(childFragmentManager,lifecycle)
            viewPager.adapter = adapter
            TabLayoutMediator(tabLayout,viewPager){tab,position->
            when(position){
                0->tab.text ="Foods"
                1->tab.text ="Sports"
                2->tab.text ="Health"
                3->tab.text ="Entertainment"

            }


            }.attach()
        }



        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}