package com.gameonanil.api_integration.ui.foodDetailFragment

import android.content.Context
import android.os.Bundle
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Space
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.LocationOn
import androidx.compose.material.icons.filled.Place
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import coil.compose.ImagePainter
import coil.compose.rememberImagePainter
import com.gameonanil.api_integration.R
import com.gameonanil.api_integration.data.foodmodel.FoodsItem
import com.gameonanil.api_integration.databinding.FragmentArticleDetailsBinding
import com.gameonanil.api_integration.databinding.FragmentFoodDetailsBinding
import com.gameonanil.api_integration.ui.articleDetailFragment.ArticleDetailsFragmentArgs


class FoodDetailsFragment : Fragment() {

    private var _binding: FragmentFoodDetailsBinding? = null
    private val binding: FragmentFoodDetailsBinding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFoodDetailsBinding.inflate(inflater, container, false)


        val animation =
            TransitionInflater.from(requireContext()).inflateTransition(android.R.transition.move)
        sharedElementEnterTransition = animation
        sharedElementReturnTransition = animation

        val id = FoodDetailsFragmentArgs.fromBundle(requireArguments()).id
        val name = FoodDetailsFragmentArgs.fromBundle(requireArguments()).name
        val address = FoodDetailsFragmentArgs.fromBundle(requireArguments()).address
        val available = FoodDetailsFragmentArgs.fromBundle(requireArguments()).available
        val price = FoodDetailsFragmentArgs.fromBundle(requireArguments()).price
        val rating = FoodDetailsFragmentArgs.fromBundle(requireArguments()).rating
        val image = FoodDetailsFragmentArgs.fromBundle(requireArguments()).image


        binding.composeView.setContent {
            FoodDetailFragmentComposable(
                FoodsItem(
                    id = id,
                    address = address,
                    available = available,
                    name = name,
                    price = price,
                    rating = rating,
                    image = image
                ),
                requireActivity()
            )
        }



        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null

    }


    @Composable
    fun FoodDetailFragmentComposable(
        food: FoodsItem,
        context: Context
    ) {
        Box(modifier = Modifier.fillMaxWidth()) {

            val resID: Int =
                context.getResources().getIdentifier(
                    food.image,
                    "drawable",
                    context.applicationContext.packageName
                )
            Image(
                painter = rememberImagePainter(resID),
                "imageViewItem",
                modifier = Modifier
                    .fillMaxWidth()
                    .height(350.dp),
                contentScale = ContentScale.Crop
            )
            Scaffold(
                modifier = Modifier.fillMaxSize(),
                topBar = {
                    TopAppBar(
                        backgroundColor = Color.Transparent,
                        elevation = 0.dp,
                        modifier = Modifier.padding(top = 14.dp)
                    ) {
                        Spacer(modifier = Modifier.height(230.dp))
                        Surface(
                            shape = CircleShape,
                            color = Color.White.copy(0.6f),
                            modifier = Modifier.size(40.dp)
                        ) {
                            IconButton(onClick = { findNavController().popBackStack() }) {
                                Icon(Icons.Default.ArrowBack, "", tint = Color.Black)
                            }
                        }
                    }
                },
                backgroundColor = Color.Transparent
            ) {
                Column {
                    Spacer(modifier = Modifier.height(230.dp))
                    Surface(
                        shape = RoundedCornerShape(topEnd = 30.dp, topStart = 30.dp),
                        modifier = Modifier
                            .fillMaxSize(),
                        color = Color.White,
                    ) {
                        Column(modifier = Modifier.padding(15.dp)) {

                            Text(
                                text = food.name,
                                style = TextStyle(
                                    color = Color.Black,
                                    fontWeight = FontWeight.Bold,
                                    fontSize = 30.sp,
                                ),
                            )
                            TextWithIcon(
                                painter = painterResource(R.drawable.ic_home),
                                imageColor = Color.Black,
                                text = food.address,
                                style = TextStyle(
                                    color = Color.Black,
                                    fontSize = 20.sp,
                                )
                            )
                            Row(
                                modifier = Modifier.fillMaxWidth(),
                                horizontalArrangement = Arrangement.Start
                            ) {

                                TextWithIcon(
                                    painter = painterResource(R.drawable.ic_car),
                                    imageColor = Color.Black,
                                    text = food.available,
                                    style = TextStyle(
                                        color = Color.Black,
                                        fontSize = 20.sp,
                                    )
                                )
                                Spacer(modifier = Modifier.width(10.dp))
                                TextWithIcon(
                                    painter = painterResource(R.drawable.ic_food),
                                    imageColor = Color.Red,
                                    text = "250 calories",
                                    style = TextStyle(
                                        color = Color.Black,
                                        fontSize = 20.sp,
                                    )
                                )
                                Spacer(modifier = Modifier.width(10.dp))
                                TextWithIcon(
                                    painter = painterResource(R.drawable.ic_star),
                                    imageColor = colorResource(id = R.color.golden),
                                    text = food.rating.toString(),
                                    style = TextStyle(
                                        color = Color.Black,
                                        fontSize = 20.sp,
                                    )
                                )
                            }
                            Spacer(modifier = Modifier.height(10.dp))
                            Text(
                                text = "Pizza is a dish of Italian origin consisting of a usually round, flat base of leavened wheat-based dough topped with tomatoes, cheese, and often various other ingredients, which is then baked at a high temperature, traditionally in a wood-fired oven. A small pizza is sometimes called a pizzetta",
                                style = TextStyle(
                                    color = Color.Black,
                                    fontSize = 20.sp,
                                ),
                            )
                            Spacer(modifier = Modifier.weight(1f))

                            Row(

                                modifier = Modifier
                                    .fillMaxWidth()
                                    .height(70.dp),
                                horizontalArrangement = Arrangement.SpaceBetween,
                                verticalAlignment = Alignment.CenterVertically
                            )
                            {
                                Box(
                                    modifier = Modifier
                                        .fillMaxHeight()
                                        .weight(1f)
                                        .clip(RoundedCornerShape(16.dp))
                                        .background(colorResource(id = R.color.goldenDark)),
                                    contentAlignment = Alignment.Center

                                ) {
                                    Row(
                                        modifier = Modifier
                                            .padding(10.dp)
                                            .fillMaxWidth()
                                            .fillMaxHeight()
                                            .background(Color.Transparent),
                                        verticalAlignment = Alignment.CenterVertically,
                                        horizontalArrangement = Arrangement.Center

                                    ) {
                                        TextButton(
                                            onClick = { /*TODO*/ },
                                            modifier = Modifier.height(70.dp)
                                        ) {
                                            Text(
                                                "-",
                                                style = TextStyle(
                                                    fontWeight = FontWeight.Bold,
                                                    fontSize = 35.sp,
                                                    color = Color.White
                                                ),
                                                modifier = Modifier.align(Alignment.CenterVertically)
                                            )
                                        }
                                        Text(
                                            "1",
                                            style = TextStyle(fontWeight = FontWeight.Bold),
                                            fontSize = 30.sp,
                                            color = Color.White,
                                            modifier = Modifier.align(Alignment.CenterVertically)
                                        )
                                        TextButton(onClick = { /*TODO*/ }) {
                                            Text(
                                                "+",
                                                style = TextStyle(fontWeight = FontWeight.Bold),
                                                fontSize = 30.sp,
                                                color = Color.White,
                                                modifier = Modifier.align(Alignment.CenterVertically)
                                            )
                                        }
                                    }
                                }
                                
                                Spacer(modifier = Modifier.width(5.dp))

                                Box(
                                    modifier = Modifier
                                        .clip(RoundedCornerShape(15.dp))
                                        .background(color = colorResource(id = R.color.orange))
                                        .fillMaxHeight()
                                        .width(80.dp),
                                    contentAlignment = Alignment.Center
                                ) {
                                    Icon(
                                        painter = painterResource(id = R.drawable.ic_cart),
                                        contentDescription = null,
                                        Modifier.size(60.dp, 60.dp),
                                        tint = Color.White
                                    )
                                }
                            }

                            Spacer(modifier = Modifier.height(20.dp))

                        }

                    }
                }
            }
        }


    }


    @Preview(showBackground = true)
    @Composable
    fun FoodDetailPreview() {
        FoodDetailFragmentComposable(
            food = FoodsItem(
                id = 1,
                address = "Radhe Radhe",
                image = "pizza_def",
                available = "30 min",
                name = "Pizza",
                price = 100,
                rating = 4.5f
            ),
            LocalContext.current
        )
    }


    @Composable
    fun TextWithIcon(
        painter: Painter,
        imageColor: Color,
        text: String,
        style: TextStyle,
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            Icon(
                painter = painter,
                contentDescription = null,
                tint = imageColor
            )
            Text(
                text = text,
                style = style,
                // modifier = modifier
            )
        }
    }

}


