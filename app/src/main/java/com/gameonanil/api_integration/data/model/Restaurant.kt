package com.gameonanil.api_integration.data.model

data class Restaurant(
    val defaultImage: String,
    val deliveryTime: String,
    val description: String,
    val id: Int,
    val location: String,
    val logo: String,
    val title: String
)