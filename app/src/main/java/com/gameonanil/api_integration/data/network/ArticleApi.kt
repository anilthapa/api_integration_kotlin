package com.gameonanil.api_integration.data.network

import com.gameonanil.api_integration.data.model.ArticleResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ArticleApi {

    @GET("v2/top-headlines")
    suspend fun getArticles(
        @Query("country") country: String,
        @Query("apiKey") apiKey: String,
        @Query("category") category: String
    ): ArticleResponse


}