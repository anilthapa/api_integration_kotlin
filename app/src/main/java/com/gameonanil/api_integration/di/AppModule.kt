package com.gameonanil.api_integration.di

import com.gameonanil.api_integration.BuildConfig
import com.gameonanil.api_integration.data.network.ArticleApi
import com.gameonanil.api_integration.data.network.FoodApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object AppModule {

    @Named("Article")
    @Provides
    @Singleton
    fun provideRetrofitArticle(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Named("Food")
    @Provides
    @Singleton
    fun provideRetrofitFood(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL_FOOD)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Provides
    @Singleton
    fun provideArticleApi(@Named("Article")retrofit: Retrofit): ArticleApi {
        return retrofit.create(
            ArticleApi::class.java
        )
    }

    @Provides
    @Singleton
    fun provideFoodApi(@Named("Food")retrofit: Retrofit): FoodApi {
        return retrofit.create(
            FoodApi::class.java
        )
    }

}