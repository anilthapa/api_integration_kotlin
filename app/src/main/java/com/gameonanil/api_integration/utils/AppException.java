package com.gameonanil.api_integration.utils;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

public class AppException extends  Exception{
    private String message;

    public static String TIMEOUT_ERROR ="Connection TimeOut";
    public static String UNKNOWN_HOST_ERROR="No Internet Connection";
    public static String CONNECT_ERROR="Connection refused by server";


    public AppException(Throwable t) {
        super(t);

        if (t instanceof UnknownHostException) {
            message = UNKNOWN_HOST_ERROR;
        } else if (t instanceof SocketTimeoutException) {
            message = TIMEOUT_ERROR;
        } else if (t instanceof ConnectException) {
            message = CONNECT_ERROR;
        }

        else {
            message = t.getMessage();
        }

    }

    @Override
    public String getMessage() {
        return message;
    }
}
