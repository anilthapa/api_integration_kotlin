package com.gameonanil.api_integration.utils

object Constants {
    const val API_KEY="7dbf093619114db49c105ed27981f718"
    const val COUNTRY="in"
    const val CATEGORY_BUSINESS ="business"
    const val CATEGORY_SPORTS ="sports"
    const val CATEGORY_ENTERTAINMENT ="entertainment"
    const val CATEGORY_HEALTH ="health"
}