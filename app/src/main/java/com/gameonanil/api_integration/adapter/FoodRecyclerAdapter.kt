package com.gameonanil.api_integration.adapter

import android.content.Context
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.gameonanil.api_integration.R
import com.gameonanil.api_integration.data.foodmodel.FoodsItem
import com.gameonanil.api_integration.databinding.FoodRecyclerItemBinding


class FoodRecyclerAdapter(
    private val context: Context,
    private val listener: MainRecyclerListener
) : RecyclerView.Adapter<FoodRecyclerAdapter.MainRecyclerViewHolder>() {
    private var foodList: MutableList<FoodsItem> = mutableListOf()

    fun setFoods(foods: List<FoodsItem>) {
        foodList.clear()
        for (food in foods) {
            foodList.add(food)

        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainRecyclerViewHolder {
        val binding =
            FoodRecyclerItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MainRecyclerViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MainRecyclerViewHolder, position: Int) {
        holder.bindTo(foodList[position])
    }

    override fun getItemCount(): Int = foodList.size


    inner class MainRecyclerViewHolder(private val binding: FoodRecyclerItemBinding) :
        RecyclerView.ViewHolder(binding.root) {


        fun bindTo(food: FoodsItem) {
            itemView.setOnClickListener {
                listener.onItemClick(food, binding.imageViewItem)

            }

            binding.apply {
                Log.d("TAG", "bindTo: current img:${food.image}")
                val uri = Uri.parse("R.drawable.image")

                val mDrawableName = food.image
                val resID: Int =
                    context.getResources().getIdentifier(
                        mDrawableName,
                        "drawable",
                        context.applicationContext.packageName
                    )
                Glide
                    .with(binding.root)
                    .load(resID)
                    .centerCrop()
                    .placeholder(R.drawable.ic_launcher_foreground)
                    .into(binding.imageViewItem)

                tvTitle.text = food.name
                tvAddress.text = food.address
                tvAvailable.text = food.available
                tvRating.text = food.rating.toString() + "⭐⭐⭐⭐⭐"
                tvPrice.text = "Rs. " + food.price.toString()

            }
        }
    }

    interface MainRecyclerListener {
        fun onItemClick(food: FoodsItem, imageView: ImageView) {

        }
    }
}
