package com.gameonanil.api_integration.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.gameonanil.api_integration.ui.homefragment.tabs.EntertainmentFragment
import com.gameonanil.api_integration.ui.homefragment.tabs.FoodFragment
import com.gameonanil.api_integration.ui.homefragment.tabs.HealthFragment
import com.gameonanil.api_integration.ui.homefragment.tabs.SportsFragment

class PagerAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) :
    FragmentStateAdapter(fragmentManager, lifecycle) {
    override fun getItemCount(): Int {
        return 4
    }

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> {
                FoodFragment()
            }
            1 -> {
                SportsFragment()
            }
            2 -> {
                HealthFragment()
            }
            3 -> {
                EntertainmentFragment()
            }
            else -> {
                Fragment()
            }
        }
    }
}