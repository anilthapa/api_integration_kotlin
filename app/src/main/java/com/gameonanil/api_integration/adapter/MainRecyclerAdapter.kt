package com.gameonanil.api_integration.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.gameonanil.api_integration.R
import com.gameonanil.api_integration.data.model.Article
import com.gameonanil.api_integration.databinding.MainRecyclerItemBinding

class MainRecyclerAdapter(
    private val listener: MainRecyclerListener
) : RecyclerView.Adapter<MainRecyclerAdapter.MainRecyclerViewHolder>() {
    private var articleList: MutableList<Article> = mutableListOf()

    fun setArticle(articles: List<Article>) {
        for (art in articles) {
            if (art.author != null && art.urlToImage.isNotEmpty()) {
                articleList.add(art)
            }
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainRecyclerViewHolder {
        val binding =
            MainRecyclerItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MainRecyclerViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MainRecyclerViewHolder, position: Int) {
        holder.bindTo(article = articleList[position])
    }

    override fun getItemCount(): Int = articleList.size


    inner class MainRecyclerViewHolder(private val binding: MainRecyclerItemBinding) :
        RecyclerView.ViewHolder(binding.root) {


        fun bindTo(article: Article) {
            itemView.setOnClickListener {
                listener.onItemClick(article,binding.imageViewItem)

            }

            binding.apply {

                Glide
                    .with(binding.root)
                    .load(article.urlToImage)
                    .centerCrop()
                    .placeholder(R.drawable.ic_launcher_foreground)
                    .into(binding.imageViewItem)
                tvTitle.text = article.author
                tvDescription.text = article.description

            }
        }
    }

    interface MainRecyclerListener {
        fun onItemClick(article: Article,imageView: ImageView) {

        }
    }
}
